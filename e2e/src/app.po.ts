import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getFavoriteButtonPresent() {
    return element(by.id('favorite-modal-button')).isPresent() as Promise<boolean>;
  }
}

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {FilterItemDTO, ItemDTO} from '../app.entities';
import {filterItems, sortItemCompareFn} from '../utils/app.utils';

@Injectable()
export class ItemsService {

  constructor(private http: HttpClient) {
  }

  getItems(): Observable<ItemDTO[]> {
    return this.http.get<ItemDTO[]>('assets/items.json', {responseType: 'json'})
      .pipe(map((itemsJson: any) => itemsJson.items.map(item => ({...item, image: `assets/${item.image}`}))));
  }

  getItemsByFilterItem(filterItemDTO: FilterItemDTO): Observable<ItemDTO[]> {
    const itemsPerPage = 5;
    return this.getItems().pipe(map((items) => {
      return filterItems(items, filterItemDTO.searchCriteria, filterItemDTO.search)
        .sort(sortItemCompareFn(filterItemDTO.orderBy))
        .slice(filterItemDTO.page * itemsPerPage, (filterItemDTO.page * itemsPerPage) + itemsPerPage);
    }));
  }
}

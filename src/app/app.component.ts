import {Component, OnDestroy} from '@angular/core';
import {AppModel} from './app.model';
import {FilterItemDTO, getSearchCriteriaValues, ItemDTO, SearchCriteria} from './app.entities';
import {MatDialog} from '@angular/material';
import {FavoriteModalComponent} from './favorite-modal/favorite-modal.component';
import {AppController} from './app.controller';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [
    AppModel,
    AppController,
    MatDialog
  ]
})
export class AppComponent implements OnDestroy {
  currentCurrency = '€';
  searchCriteriaValues = getSearchCriteriaValues();

  favoritesDictionary: { [id: string]: ItemDTO } = {};
  private favoritesSubscription: Subscription;

  constructor(public appModel: AppModel,
              private appController: AppController,
              public dialog: MatDialog) {

    this.favoritesSubscription = this.appModel.favoritesList$.subscribe((favoritesList: ItemDTO[]) => {
      // convert it into a map indexed by its identifier, for having fast access and improve performance.
      this.favoritesDictionary = {};
      favoritesList.forEach((favorite) => this.favoritesDictionary[favorite.id] = favorite);
    });
  }

  ngOnDestroy() {
    this.favoritesSubscription.unsubscribe();
  }

  trackByFn(index, item: ItemDTO) {
    return item.id;
  }

  isItemFavorite(item: ItemDTO) {
    return this.favoritesDictionary[item.id] != null;
  }

  onFavoriteClicked() {
    this.openDialog();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(FavoriteModalComponent, {
      width: '600px',
      height: '750px'
    });
  }

  onSearchInputChange(value: string) {
    this.appController.changeSearchValue(value);
  }

  showMoreItems() {
    this.appController.getMoreItems();
  }

  onSearchCriteriaChange(criteria: SearchCriteria) {
    this.appController.changeSearchCriteria(criteria);
  }

  onOrderByChange(orderBy: SearchCriteria) {
    this.appController.changeOrderBy(orderBy);
  }

  toggleItemToFavorites(item: ItemDTO) {
    this.appController.toggleFavorite(item);
  }
}

import {ItemDTO} from '../app.entities';

/**
 * Returns a filtered items []
 * @param items [] source in which the filter would be applied
 * @param filterByKey is the target key of the ItemDTO in order to perform the filter
 * @param filterValue is the value to compare (the current filter).
 */
export function filterItems(items: ItemDTO[], filterByKey: keyof ItemDTO, filterValue: string): ItemDTO[] {
  return items.filter((item) => item[filterByKey].toLowerCase().includes(filterValue.toLowerCase()));
}

/**
 * Given a ItemDTO key, return a compareFn from two ItemDTOs that can be used for example as compareFn of Array.prototype.sort.
 * @param sortByKey key in which this item would be sort.
 */
export const sortItemCompareFn = (sortByKey: keyof ItemDTO) => (itemA: ItemDTO, itemB: ItemDTO): number => {
  // if properties are numbers, make number comparision.
  if (!isNaN(itemA[sortByKey] as any) && !isNaN(itemB[sortByKey] as any)) {
    return parseInt(itemA[sortByKey], 10) - parseInt(itemB[sortByKey], 10);
  }

  // if properties are string, just make an alphabetic string comparision
  if (typeof itemA[sortByKey] === 'string' && typeof itemB[sortByKey] === 'string') {
    if (itemA[sortByKey].toLowerCase() < itemB[sortByKey].toLowerCase()) {
      return -1;
    }
    if (itemA[sortByKey].toLowerCase() > itemB[sortByKey].toLowerCase()) {
      return 1;
    }
    return 0;
  }

  return 0;
};

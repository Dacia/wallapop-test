import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {SearchBarModule} from './components/search-bar/search-bar.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatDialogModule, MatFormFieldModule, MatIconModule, MatOptionModule, MatSelectModule} from '@angular/material';
import {CardItemModule} from './components/card-item/card-item.module';
import {ItemsService} from './services/items.service';
import {HttpClientModule} from '@angular/common/http';
import { FavoriteModalComponent } from './favorite-modal/favorite-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    FavoriteModalComponent
  ],
  entryComponents: [FavoriteModalComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    SearchBarModule,
    MatSelectModule,
    MatOptionModule,
    MatFormFieldModule,
    CardItemModule,
    HttpClientModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule
  ],
  providers: [
    ItemsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import {Component, OnInit, ViewChild} from '@angular/core';
import {AppModel} from '../app.model';
import {AppController} from '../app.controller';
import {ItemDTO, SearchCriteria} from '../app.entities';
import {SearchBarComponent} from '../components/search-bar/search-bar.component';
import {combineLatest, Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {filterItems} from '../utils/app.utils';

@Component({
  selector: 'app-favorite-modal',
  templateUrl: './favorite-modal.component.html',
  styleUrls: ['./favorite-modal.component.scss']
})
export class FavoriteModalComponent implements OnInit {
  @ViewChild('searchBar') searchBar: SearchBarComponent;

  favoritesListFiltered$: Observable<ItemDTO[]>;

  constructor(private appModel: AppModel, private appController: AppController) {
  }

  ngOnInit() {
    this.favoritesListFiltered$ = combineLatest(
      this.appModel.favoritesList$,
      this.searchBar.inputChange.pipe(startWith(''))
    ).pipe(map(([favoriteList, searchValue]: [ItemDTO[], string]) =>
      filterItems(favoriteList, SearchCriteria.Title, searchValue)
    ));
  }

  trackByFn(index, item: ItemDTO) {
    return item.id;
  }

  removeFavorite(item: ItemDTO) {
    this.appController.toggleFavorite(item);
  }

}

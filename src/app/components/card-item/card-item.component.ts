import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-card-item',
  templateUrl: './card-item.component.html',
  styleUrls: ['./card-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardItemComponent implements OnInit {
  @Input() title: string;
  @Input() description: string;
  @Input() email: string;
  @Input() image: string;
  @Input() price: string;

  constructor() { }

  ngOnInit() {
  }

}

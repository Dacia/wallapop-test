import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardItemComponent } from './card-item.component';
import {MatButtonModule, MatCardModule} from '@angular/material';

@NgModule({
  declarations: [CardItemComponent],
  exports: [CardItemComponent],
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule
  ]
})
export class CardItemModule { }

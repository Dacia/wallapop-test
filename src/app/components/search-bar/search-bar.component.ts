import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {fromEvent, Observable, Subscription} from 'rxjs';
import {debounceTime, map} from 'rxjs/operators';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchBarComponent implements OnInit, OnDestroy {
  @ViewChild('input') input: ElementRef;
  @Input() placeholder = 'Search';
  @Input() value = '';

  @Input()
  set debounceTime(time: number) {
    this._debounceTime = time;

    if (this.inputSubscription) {
      this.inputSubscription.unsubscribe();
    }
    const input$: Observable<string> = fromEvent(this.input.nativeElement, 'input').pipe(
      map((keyboardEvent: any) => keyboardEvent.target.value)
    );
    const inputDebounced$: Observable<string> = this.debounceTime >= 0 ? input$.pipe(debounceTime(this.debounceTime)) : input$;
    this.inputSubscription = inputDebounced$.subscribe(value => {
        this.inputChange.next(value);
      });
  }
  get debounceTime(): number {
    return this._debounceTime;
  }

  /** Output event emitter for input changes */
  @Output() inputChange: EventEmitter<string> = new EventEmitter<string>();

  private _debounceTime: number;
  private inputSubscription: Subscription;

  constructor() { }

  ngOnInit() {
    /**
     * if there isn't any inputSubscription, it means that no [debounceTime] input has been passed to the component.
     * DebounceTime is mandatory on this component in order to init the inputChange subscription, so we must init it with a value.
     * This is a little bit 'tricky' but for now there aren't much solutions provided for the Angular community,
     * see: https://stackoverflow.com/questions/43111474/how-to-stop-ngonchanges-called-before-ngoninit
     */
    if (!this.inputSubscription) {
      this.debounceTime = null;
    }
  }

  ngOnDestroy() {
    this.inputSubscription.unsubscribe();
  }

}

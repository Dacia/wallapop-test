import {BehaviorSubject, Observable} from 'rxjs';
import {distinctUntilChanged, pluck} from 'rxjs/operators';
import {ItemDTO, SearchCriteria} from './app.entities';

export interface AppModelData {
  search: string;
  searchCriteria: SearchCriteria;
  orderBy: SearchCriteria;
  lastPage: number;
  itemsList: any[];
  favoritesList: any[];
}

const initialValue: AppModelData = {
  search: '',
  searchCriteria: SearchCriteria.Title,
  orderBy: SearchCriteria.Price,
  lastPage: -1,
  itemsList: [],
  favoritesList: []
};

export class AppModel {
  public search$: Observable<string>;
  public searchCriteria$: Observable<SearchCriteria>;
  public orderBy$: Observable<SearchCriteria>;
  public itemsList$: Observable<ItemDTO[]>;
  public favoritesList$: Observable<ItemDTO[]>;
  private model: BehaviorSubject<AppModelData>;

  constructor() {
    this.model = new BehaviorSubject<AppModelData>(initialValue);
    this.search$ = this.getProperty('search');
    this.searchCriteria$ = this.getProperty('searchCriteria');
    this.orderBy$ = this.getProperty('orderBy');
    this.itemsList$ = this.getProperty('itemsList');
    this.favoritesList$ = this.getProperty('favoritesList');
  }

  update(value: AppModelData) {
    this.model.next(value);
  }

  updateProperty(propertyName: keyof AppModelData, value: any) {
    this.update({
      ...this.getCurrentValue(),
      [propertyName]: value
    });
  }

  get model$(): Observable<AppModelData> {
    return this.model.asObservable();
  }

  getCurrentValue(): AppModelData {
    return this.model.value;
  }

  getProperty(...keys): Observable<any> {
    return this.model.pipe(pluck(...keys), distinctUntilChanged());
  }
}

export enum SearchCriteria {
  Title = 'title',
  Description = 'description',
  Price = 'price',
  Email = 'email'
}

export interface ItemDTO {
  id: string;
  title: string;
  description: string;
  price: string;
  email: string;
  image: string;
}

export function getSearchCriteriaValues() {
  return Object.values(SearchCriteria);
}

export interface FilterItemDTO {
  search: string;
  searchCriteria: SearchCriteria;
  orderBy: SearchCriteria;
  page: number;
}

import {ItemDTO} from '../app.entities';
import {getMockItemData} from './mock-data';
import {sortItemCompareFn} from '../utils/app.utils';


describe('Utils tests', () => {

  describe('sortItemCompareFn tests', () => {

    let items: ItemDTO[];
    beforeEach(() => {
      items = getMockItemData();
    });

    it('should orderBy title in alphabetic order', () => {
      const itemsSorted: ItemDTO[] = items.sort(sortItemCompareFn('title'));
      expect(itemsSorted.map(item => item.title)).toEqual([
        'Bolso piel marca Hoss',
        'Coche antiguo americano',
        'iPhone 6S Oro',
        'Polaroid 635',
        'Reloj de Daniel Wellington'
      ]);
    });

    it('should orderBy number in numeric order', () => {
      const itemsSorted: ItemDTO[] = items.sort(sortItemCompareFn('price'));
      expect(itemsSorted.map(item => item.price)).toEqual(['50', '100', '250', '740', '210000']);
    });
  });
});

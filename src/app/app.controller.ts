import {Injectable, OnDestroy} from '@angular/core';
import {AppModel, AppModelData} from './app.model';
import {FilterItemDTO, ItemDTO, SearchCriteria} from './app.entities';
import {ItemsService} from './services/items.service';
import {xorBy} from 'lodash-es';
import {Observable, of, Subject} from 'rxjs';
import {catchError, map, switchMap} from 'rxjs/operators';

@Injectable()
export class AppController implements OnDestroy {
  private searchActionsStream: Subject<FilterItemDTO> = new Subject();
  constructor(private appModel: AppModel, private itemsService: ItemsService) {

    /**
     * Subscribe to searchActionsStreams. This way to deal with the logic, let us abort previous 'call' if not finished. This way, we
     * can avoid any race condition that could happen, even in this case are remote to happen.
     */
    this.searchActionsStream.pipe(switchMap((filterItemDTO: FilterItemDTO) => {
      this.appModel.update({
        ...this.appModel.getCurrentValue(),
        search: filterItemDTO.search,
        searchCriteria: filterItemDTO.searchCriteria,
        orderBy: filterItemDTO.orderBy,
      });
      return this.performSearchAndGetNewModel(filterItemDTO);
    })).subscribe((model: AppModelData) => {
      this.appModel.update(model);
    });

    // Init the search with the current model values
    this.search({
      search: this.appModel.getCurrentValue().search,
      searchCriteria: this.appModel.getCurrentValue().searchCriteria,
      orderBy: this.appModel.getCurrentValue().orderBy,
      page: 0
    });

  }

  ngOnDestroy() {
    this.searchActionsStream.complete();
  }

  /**
   * Performs a search with the new search value. Also reset pagination to 0.
   */
  changeSearchValue(value: string) {
    this.search({
      search: value,
      searchCriteria: this.appModel.getCurrentValue().searchCriteria,
      orderBy: this.appModel.getCurrentValue().orderBy,
      page: 0
    });
  }

  /**
   * Performs a search with the new search criteria. Also reset pagination to 0.
   */
  changeSearchCriteria(criteria: SearchCriteria) {
    this.search({
      search: this.appModel.getCurrentValue().search,
      searchCriteria: criteria,
      orderBy: this.appModel.getCurrentValue().orderBy,
      page: 0
    });
  }

  /**
   * Performs a search with the new orderBy value. Also reset pagination to 0.
   */
  changeOrderBy(orderBy: SearchCriteria) {
    this.search({
      search: this.appModel.getCurrentValue().search,
      searchCriteria: this.appModel.getCurrentValue().searchCriteria,
      orderBy,
      page: 0
    });
  }

  /**
   * Gets the items that are on the next page given the current filter values
   */
  getMoreItems() {
    this.search({
      search: this.appModel.getCurrentValue().search,
      searchCriteria: this.appModel.getCurrentValue().searchCriteria,
      orderBy: this.appModel.getCurrentValue().orderBy,
      page: this.appModel.getCurrentValue().lastPage + 1
    });
  }

  /**
   * Performs a toggle favorite action. If the favorite is present removes it, if not, adds it.
   * @param item favorite to be added or removed
   */
  toggleFavorite(item: ItemDTO) {
    this.appModel.update({
      ...this.appModel.getCurrentValue(),
      favoritesList: xorBy(this.appModel.getCurrentValue().favoritesList, [item], 'id')
    });
  }

  /**
   * Puts this filterItemDTO into the search pipeline
   */
  search(filterItemDTO: FilterItemDTO) {
    this.searchActionsStream.next(filterItemDTO);
  }

  /**
   * Performs a search request with a filterItemDTO and returns the new model data. This observable never gives error.
   */
  private performSearchAndGetNewModel(filterItemDTO: FilterItemDTO): Observable<AppModelData> {
    return this.itemsService.getItemsByFilterItem(filterItemDTO).pipe(map((items) => {
        return {
          ...this.appModel.getCurrentValue(),
          itemsList: filterItemDTO.page === 0 ? items : [...this.appModel.getCurrentValue().itemsList, ...items],
          lastPage: filterItemDTO.page
        };
      }),
      catchError(() => {
        // TODO: Handle a possible network/server error and update the model accordingly.
        return of({...this.appModel.getCurrentValue()});
      }));
  }
}

# WallapopTest

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.8.

In order to install the dependencies you can simply do **npm install**

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build (ES5 target)

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

Run `npm run build_serve` in order to build the project and also serve it!. In order to test easialy the final build

## Running unit tests

Run `npm run test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `npm run e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
